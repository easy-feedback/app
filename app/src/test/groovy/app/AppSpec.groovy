package app

import io.micronaut.runtime.EmbeddedApplication
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import org.testcontainers.containers.PostgreSQLContainer
import spock.lang.Shared
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class AppSpec extends Specification {

    @Inject
    EmbeddedApplication<?> application

    @Shared
    PostgreSQLContainer container = new PostgreSQLContainer("postgres:11.1");

    void 'test it works'() {
        expect:
        application.running
    }

}
