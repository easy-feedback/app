package app

import app.model.EasyFeedback
import app.model.Question
import app.port.jdbc.JdbcEasyFeedbackRepository
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.runtime.EmbeddedApplication
import io.micronaut.security.authentication.UsernamePasswordCredentials
import io.micronaut.security.token.jwt.render.BearerAccessRefreshToken
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import org.testcontainers.containers.PostgreSQLContainer
import spock.lang.Shared
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class FeedbackControllerSpec extends Specification {

    @Inject
    EmbeddedApplication<?> application

    @Shared
    PostgreSQLContainer container = new PostgreSQLContainer("postgres:11.1");

    @Inject
    @Client('/')
    RxHttpClient client

    @Inject
    JdbcEasyFeedbackRepository repository

    String login(){
        UsernamePasswordCredentials creds = new UsernamePasswordCredentials("sherlock", "password")
        HttpRequest request = HttpRequest.POST('/login', creds)
        HttpResponse<BearerAccessRefreshToken> rsp = client.toBlocking().exchange(request, BearerAccessRefreshToken)
        BearerAccessRefreshToken bearerAccessRefreshToken = rsp.body()
        return bearerAccessRefreshToken.accessToken
    }

    def cleanup() {
        repository.deleteAll()
    }

    void 'test start'() {
        given: "have an easyfeedback"
        EasyFeedback instance = new EasyFeedback(
                title: "test",
                questions: [new Question(question: "test1", votes: 0), new Question(question: "test2", votes: 0)],
                state: "OPEN"
        )

        when: "create"
        EasyFeedback ret = client.toBlocking()
                .retrieve(HttpRequest.POST('/api/edit', instance).bearerAuth(login()), EasyFeedback)

        and: "start"
        HttpResponse start = client.toBlocking()
                .exchange(HttpRequest.GET("/api/moderator/$ret.id/start").bearerAuth(login()))

        then: "check"
        start.status() == HttpStatus.OK

        and:
        repository.findById(ret.id).get().state == 'STARTED'
    }

    void 'test stop'() {
        given: "have an easyfeedback"
        EasyFeedback instance = new EasyFeedback(
                title: "test",
                questions: [new Question(question: "test1", votes: 0), new Question(question: "test2", votes: 0)],
                state: "STARTED"
        )

        when: "create"
        EasyFeedback ret = client.toBlocking()
                .retrieve(HttpRequest.POST('/api/edit', instance).bearerAuth(login()), EasyFeedback)

        and: "start"
        HttpResponse start = client.toBlocking()
                .exchange(HttpRequest.GET("/api/moderator/$ret.id/stop").bearerAuth(login()))

        then: "check"
        start.status() == HttpStatus.OK

        and:
        repository.findById(ret.id).get().state == 'CLOSED'
    }

    void 'test restarted'() {
        given: "have an easyfeedback"
        EasyFeedback instance = new EasyFeedback(
                title: "test",
                questions: [new Question(question: "test1", votes: 1), new Question(question: "test2", votes: 0)],
                state: "CLOSED"
        )

        when: "create"
        EasyFeedback ret = client.toBlocking()
                .retrieve(HttpRequest.POST('/api/edit', instance).bearerAuth(login()), EasyFeedback)

        and: "start"
        HttpResponse start = client.toBlocking()
                .exchange(HttpRequest.GET("/api/moderator/$ret.id/reset").bearerAuth(login()))

        then: "check"
        start.status() == HttpStatus.OK

        and:
        repository.findById(ret.id).get().state == 'CLOSED'
        repository.findById(ret.id).get().questions*.votes.sort() == [0,0]
    }

}
