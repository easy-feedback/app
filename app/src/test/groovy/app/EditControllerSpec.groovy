package app

import app.model.EasyFeedback
import app.model.Question
import app.port.jdbc.JdbcEasyFeedbackRepository
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.runtime.EmbeddedApplication
import io.micronaut.security.authentication.UsernamePasswordCredentials
import io.micronaut.security.token.jwt.render.BearerAccessRefreshToken
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import org.testcontainers.containers.PostgreSQLContainer
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

import javax.inject.Inject

@Stepwise
@MicronautTest
class EditControllerSpec extends Specification {

    @Inject
    EmbeddedApplication<?> application

    @Shared
    PostgreSQLContainer container = new PostgreSQLContainer("postgres:11.1");

    @Inject
    @Client('/')
    RxHttpClient client

    @Shared
    String accessToken

    @Inject
    JdbcEasyFeedbackRepository repository

    void 'test login'(){
        when: 'Login endpoint is called with valid credentials'
        UsernamePasswordCredentials creds = new UsernamePasswordCredentials("sherlock", "password")
        HttpRequest request = HttpRequest.POST('/login', creds)
        HttpResponse<BearerAccessRefreshToken> rsp = client.toBlocking().exchange(request, BearerAccessRefreshToken)

        then: 'the endpoint can be accessed'
        rsp.status == HttpStatus.OK

        when:
        BearerAccessRefreshToken bearerAccessRefreshToken = rsp.body()

        then:
        bearerAccessRefreshToken.username == 'sherlock'

        when: 'passing the access token as in the Authorization HTTP Header with the prefix Bearer allows the user to access a secured endpoint'
        accessToken = bearerAccessRefreshToken.accessToken

        then:
        accessToken
    }

    void 'test create'() {
        given: "have an easyfeedback"
        EasyFeedback instance = new EasyFeedback(
                title: "test",
                questions: [ new Question(question: "test1", votes: 0), new Question(question: "test2", votes: 0)],
                state: "OPEN"
        )

        when: "create"
        EasyFeedback ret = client.toBlocking()
                .retrieve(HttpRequest.POST('/api/edit', instance).bearerAuth(accessToken), EasyFeedback)

        then: "check"
        ret.id
        ret.author == "sherlock"

        and:
        repository.findAllByAuthor("sherlock").size() == 1
    }

    void 'test list'(){
        when: "list"
        List<EasyFeedback> ret = client.toBlocking()
                .retrieve(HttpRequest.GET('/api/edit').bearerAuth(accessToken), List<EasyFeedback>)

        then: "retrieve an element"
        ret.size() == 1
        ret.first().author == 'sherlock'
        ret.first().questions.size()==2
    }

    void 'test update'() {
        given: "have an easyfeedback"
        List<EasyFeedback> list = client.toBlocking()
                .retrieve(HttpRequest.GET('/api/edit').bearerAuth(accessToken), List<EasyFeedback>)
        EasyFeedback update = list.first()

        when: "add question"
        update.questions.add(new Question(question: "test3"))
        EasyFeedback ret = client.toBlocking()
                .retrieve(HttpRequest.PUT("/api/edit/${update.id}", update).bearerAuth(accessToken), EasyFeedback)

        then: "check"
        client.toBlocking()
                .retrieve(HttpRequest.GET('/api/edit').bearerAuth(accessToken), List<EasyFeedback>).size()==1
        ret.id == update.id
        ret.author == "sherlock"
        ret.questions.size() == 3
        ret.questions*.question.size() == 3
    }

    void 'test update question'() {
        given: "have an easyfeedback"
        List<EasyFeedback> list = client.toBlocking()
                .retrieve(HttpRequest.GET('/api/edit').bearerAuth(accessToken), List<EasyFeedback>)
        EasyFeedback update = list.first()

        when: "add question"
        update.questions.first().question = 'changed'
        EasyFeedback ret = client.toBlocking()
                .retrieve(HttpRequest.PUT("/api/edit/${update.id}", update).bearerAuth(accessToken), EasyFeedback)

        then: "check"
        ret.id == update.id
        ret.author == "sherlock"
        ret.questions.size() == update.questions.size()
        ret.questions.first().question == 'changed'
    }

    void 'test delete question'() {
        given: "have an easyfeedback"
        List<EasyFeedback> list = client.toBlocking()
                .retrieve(HttpRequest.GET('/api/edit').bearerAuth(accessToken), List<EasyFeedback>)
        EasyFeedback update = list.first()

        when: "remove question"
        update.questions = update.questions.dropRight(1)
        EasyFeedback ret = client.toBlocking()
                .retrieve(HttpRequest.PUT("/api/edit/${update.id}", update).bearerAuth(accessToken), EasyFeedback)

        then: "check"
        ret.id == update.id
        ret.author == "sherlock"
        ret.questions.size() == update.questions.size()
        ret.questions.first().question == 'changed'
    }

    void 'test delete'() {
        given: "have an easyfeedback"
        List<EasyFeedback> list = client.toBlocking()
                .retrieve(HttpRequest.GET('/api/edit').bearerAuth(accessToken), List<EasyFeedback>)
        EasyFeedback update = list.first()

        when: "delete"
        HttpResponse response = client.toBlocking()
                .exchange(HttpRequest.DELETE("/api/edit/${update.id}").bearerAuth(accessToken))

        then: "check"
        response.status() == HttpStatus.OK
        repository.findAllByAuthor("sherlock").size() == 0
    }
}
