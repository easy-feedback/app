package app.usecases

import app.model.EasyFeedback
import app.port.EasyFeedbackEntity
import app.port.EasyFeedbackRepository

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ListEasyFeedbackUseCase implements UseCase<String, List<EasyFeedback>>{

    @Inject
    EasyFeedbackRepository repository

    @Override
    List<EasyFeedback> execute(String author) {
        repository.findAllByAuthor(author).inject([],{ List<EasyFeedbackEntity> list, EasyFeedbackEntity entity->
            list.add entity.toEasyFeedback()
            list
        })
    }
}
