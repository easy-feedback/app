package app.usecases

import app.model.EasyFeedback
import app.port.EasyFeedbackEntity
import app.port.EasyFeedbackRepository

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UpdateEasyFeedbackUseCase implements UseCase<EasyFeedback, EasyFeedback>{

    @Inject
    EasyFeedbackRepository repository

    @Override
    EasyFeedback execute(EasyFeedback input) {
        EasyFeedbackEntity entity = repository.findById(input.id).orElseThrow()
        entity.title = input.title
        entity.questions = input.questions
        entity = repository.update(entity)
        entity.toEasyFeedback()
    }
}
