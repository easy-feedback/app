package app.usecases


import app.port.EasyFeedbackEntity
import app.port.EasyFeedbackRepository

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DeleteEasyFeedbackUseCase implements UseCase<String, Void>{

    @Inject
    EasyFeedbackRepository repository

    @Override
    Void execute(String id) {
        Optional<EasyFeedbackEntity> entity = repository.findById(id)
        if( entity.present )
            repository.delete( entity.get().id.toString() )
    }
}
