package app.usecases

import app.model.Command
import app.model.Question
import app.port.EasyFeedbackEntity
import app.port.EasyFeedbackRepository

import groovy.transform.CompileDynamic

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CommandUseCase implements UseCase<Command, Boolean>{

    @Inject
    EasyFeedbackRepository repository

    @CompileDynamic
    @Override
    Boolean execute(Command command) {
        Optional<EasyFeedbackEntity> optional = repository.findById(command.id)
        if(!optional.present)
            return false

        EasyFeedbackEntity easyfeedback=optional.get()

        Boolean ret = false
        switch (command.action) {
            case Command.START:
                if( ret=startFeedBack(easyfeedback, command.extra) ){
                    repository.updateState(easyfeedback)
                }
                break
            case Command.STOP:
                if( ret=stopFeedBack(easyfeedback, command.extra) ){
                    repository.updateState(easyfeedback)
                }
                break
            case Command.RESET:
                if( ret = resetFeedBack(easyfeedback, command.extra) ){
                    repository.update(easyfeedback)
                }
                break
            case Command.VOTE:
                if(ret = voteFeedBack(easyfeedback, command.extra)){
                    repository.update(easyfeedback)
                }
                break
        }
        return ret
    }

    boolean startFeedBack(EasyFeedbackEntity easyfeedback, String user){
        if( easyfeedback.author == user ) {
            easyfeedback.state = "STARTED"
            return true
        }
        false
    }

    boolean stopFeedBack(EasyFeedbackEntity easyfeedback, String user){
        if( easyfeedback.author == user ) {
            if (easyfeedback.state && easyfeedback.state == 'STARTED') {
                easyfeedback.state = "CLOSED"
                return true
            }
        }
        false
    }


    boolean resetFeedBack(EasyFeedbackEntity easyfeedback, String user){
        if( easyfeedback.author == user ) {
            if (easyfeedback.state && easyfeedback.state == 'CLOSED') {
                List<Question> list = easyfeedback.questions
                list.each{
                    it.votes = 0
                }
                easyfeedback.questions = list
                return true
            }
        }
        false
    }

    boolean voteFeedBack(EasyFeedbackEntity easyfeedback, String extra){
        if( easyfeedback.state && easyfeedback.state == 'STARTED' && extra ){
            List<Question> list = easyfeedback.questions
            list.find{it.question==extra }?.votes++
            easyfeedback.questions = list
            return true
        }
        false
    }
}
