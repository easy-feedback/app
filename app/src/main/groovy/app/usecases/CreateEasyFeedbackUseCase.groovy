package app.usecases

import app.model.EasyFeedback
import app.port.EasyFeedbackEntity
import app.port.EasyFeedbackRepository

import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
@Named("createEasyFeedbackUseCase")
class CreateEasyFeedbackUseCase implements UseCase<EasyFeedback, EasyFeedback>{

    @Inject
    EasyFeedbackRepository easyFeedbackRepository

    @Override
    EasyFeedback execute(EasyFeedback input) {
        EasyFeedbackEntity entity = EasyFeedbackEntity.fromEasyFeedback(input)
        easyFeedbackRepository.save(entity).toEasyFeedback()
    }
}
