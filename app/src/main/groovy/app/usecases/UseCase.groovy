package app.usecases

interface UseCase<I, O>  {

    O execute( I input )

}
