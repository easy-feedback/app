package app.usecases

import app.model.EasyFeedback
import app.port.EasyFeedbackEntity
import app.port.EasyFeedbackRepository

import javax.inject.Inject

class FindUseCase implements UseCase<String, Optional<EasyFeedback>>{

    @Inject
    EasyFeedbackRepository repository

    @Override
    Optional<EasyFeedback> execute(String input) {
        Optional<EasyFeedbackEntity> entity = repository.findById(input)
        entity.isPresent() ? Optional.of(entity.get().toEasyFeedback()) : Optional.empty()
    }
}
