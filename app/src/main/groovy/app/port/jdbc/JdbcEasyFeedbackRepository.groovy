package app.port.jdbc

import app.port.EasyFeedbackEntity
import app.port.EasyFeedbackRepository
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.Join
import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.model.query.builder.sql.Dialect
import io.micronaut.data.repository.CrudRepository

import javax.inject.Inject
import javax.transaction.Transactional

@JdbcRepository(dialect = Dialect.POSTGRES)
abstract class JdbcEasyFeedbackRepository implements CrudRepository<EasyFeedbackEntity, UUID>, EasyFeedbackRepository {

    Optional<EasyFeedbackEntity> findById(String id){
        findById(UUID.fromString(id))
    }

    abstract Optional<EasyFeedbackEntity> findById(UUID id)

    abstract Optional<EasyFeedbackEntity> findByAuthor(String author)

    @Transactional
    abstract EasyFeedbackEntity save(EasyFeedbackEntity easyFeedbackEntity)

    @Transactional
    abstract EasyFeedbackEntity update(EasyFeedbackEntity easyFeedbackEntity)

    @Transactional
    EasyFeedbackEntity updateState(EasyFeedbackEntity easyFeedbackEntity){
        update(easyFeedbackEntity.id, easyFeedbackEntity.state)
        easyFeedbackEntity
    }

    abstract void update(@Id UUID id, String state)

    abstract List<EasyFeedbackEntity> findAllByAuthor(String author)

    @Transactional
    abstract void delete(String id)

    @Transactional
    abstract void delete(EasyFeedbackEntity entity)
}
