package app.port

interface EasyFeedbackRepository {

    Optional<EasyFeedbackEntity> findById(String id)

    Optional<EasyFeedbackEntity> findByAuthor(String author)

    EasyFeedbackEntity save(EasyFeedbackEntity easyFeedbackEntity)

    EasyFeedbackEntity update(EasyFeedbackEntity easyFeedbackEntity)

    EasyFeedbackEntity updateState(EasyFeedbackEntity easyFeedbackEntity)

    List<EasyFeedbackEntity> findAllByAuthor(String author)

    void delete(String id)

    void delete(EasyFeedbackEntity entity)
}