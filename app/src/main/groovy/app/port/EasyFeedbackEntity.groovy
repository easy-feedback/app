package app.port

import app.model.EasyFeedback
import app.model.Question
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import io.micronaut.data.annotation.AutoPopulated
import io.micronaut.data.annotation.MappedEntity

import javax.persistence.Column
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.Transient

@MappedEntity
@Table(name = "feedback")
class EasyFeedbackEntity {

    @Id
    @AutoPopulated
    UUID id

    String author

    String title

    @Column(length = 2048)
    String rawQuestions

    String state

    @Transient
    List<Question> getQuestions(){
        def json = new JsonSlurper()
        List<Question> ret = json.parseText(rawQuestions ?: '[]') as List<Question>
        ret
    }

    void setQuestions(List<Question>list){
        rawQuestions = JsonOutput.prettyPrint(JsonOutput.toJson(list))
    }

    static EasyFeedbackEntity fromEasyFeedback(EasyFeedback easyFeedback){
        EasyFeedbackEntity ret = new EasyFeedbackEntity(
                id: easyFeedback.id ? UUID.fromString(easyFeedback.id) : null,
                author: easyFeedback.author,
                title: easyFeedback.title,
                state: easyFeedback.state,
        )
        ret.setQuestions(easyFeedback.questions)
        ret
    }

    EasyFeedback toEasyFeedback(){
        EasyFeedback ret = new EasyFeedback(
                id:id.toString(),
                author: author,
                title: title,
                state: state,
                questions: getQuestions()
        )
        ret
    }

}
