package app.model

import groovy.transform.ToString

@ToString
class EasyFeedback {

    String id

    String author

    String title

    List<Question> questions = []

    String state = "CLOSED"

}
