package app.model

import groovy.transform.builder.Builder

@Builder
class Command {

    static final String START="START"
    static final String STOP="STOP"
    static final String RESET="RESET"
    static final String VOTE="VOTE"

    String id

    String action

    String extra

}
