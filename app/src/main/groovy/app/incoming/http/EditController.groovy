package app.incoming.http

import app.model.EasyFeedback
import app.usecases.CreateEasyFeedbackUseCase
import app.usecases.DeleteEasyFeedbackUseCase
import app.usecases.FindUseCase
import app.usecases.ListEasyFeedbackUseCase
import app.usecases.UpdateEasyFeedbackUseCase
import io.micronaut.http.annotation.*
import io.micronaut.security.annotation.Secured
import io.micronaut.security.authentication.Authentication
import io.micronaut.security.rules.SecurityRule
import io.reactivex.Single

import javax.inject.Inject

@Controller('/api/edit')
class EditController {

    @Inject
    ListEasyFeedbackUseCase listEasyFeedbackUseCase
    @Inject
    CreateEasyFeedbackUseCase createEasyFeedbackUseCase
    @Inject
    FindUseCase findUseCase
    @Inject
    UpdateEasyFeedbackUseCase updateEasyFeedbackUseCase
    @Inject
    DeleteEasyFeedbackUseCase deleteEasyFeedbackUseCase

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get('/{id}')
    Optional<EasyFeedback> findEasyFeedback(final String id){
        findUseCase.execute(id)
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get
    List<EasyFeedback> listEasyFeedback(final Authentication authentication){
        listEasyFeedbackUseCase.execute(authentication.name)
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Post
    Single<EasyFeedback> createEasyFeedback(final Authentication authentication, final EasyFeedback easyFeedback){
        easyFeedback.author = authentication.name
        Single.create{emitter->
            emitter.onSuccess createEasyFeedbackUseCase.execute(easyFeedback)
        }
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Put('/{id}')
    Single<EasyFeedback> updateEasyFeedback(final String id, final EasyFeedback easyFeedback){
        Single.create{emitter->
            emitter.onSuccess updateEasyFeedbackUseCase.execute(easyFeedback)
        }
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Delete('/{id}')
    void deleteEasyFeedback(final String id){
        deleteEasyFeedbackUseCase.execute(id)
    }

}
