package app.incoming.http

import app.model.Command
import app.model.EasyFeedback
import app.usecases.CommandUseCase
import app.usecases.FindUseCase
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Produces
import io.micronaut.http.sse.Event
import io.micronaut.security.annotation.Secured
import io.micronaut.security.authentication.Authentication
import io.micronaut.security.rules.SecurityRule
import io.reactivex.Flowable

import javax.inject.Inject
import java.util.concurrent.TimeUnit

@Secured(SecurityRule.IS_ANONYMOUS)
@Controller('/api/public')
class AnonymousController {

    @Inject
    CommandUseCase commandUseCase

    @Inject
    FindUseCase findUseCase

    @Get('/{id}')
    Optional<EasyFeedback> findEasyFeedback(final String id){
        findUseCase.execute(id)
    }

    @Produces(MediaType.TEXT_EVENT_STREAM)
    @Get("/{id}/sse")
    Flowable<Event<EasyFeedback>> sse(String id){
        Flowable<Event<EasyFeedback>>.fromCallable({
            Optional<EasyFeedback> optional = findUseCase.execute(id)
            optional.orElse(new EasyFeedback())
        }).repeat().delay(1, TimeUnit.SECONDS)
    }

    @Post("{id}/vote")
    boolean vote(String id, String question){
        Command command = Command.builder().id(id).action(Command.VOTE).extra(question).build()
        commandUseCase.execute(command)
    }
}
