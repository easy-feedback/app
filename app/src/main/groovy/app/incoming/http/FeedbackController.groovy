package app.incoming.http

import app.model.Command
import app.model.EasyFeedback
import app.usecases.CommandUseCase
import app.usecases.FindUseCase
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Produces
import io.micronaut.http.sse.Event
import io.micronaut.security.annotation.Secured
import io.micronaut.security.authentication.Authentication
import io.micronaut.security.rules.SecurityRule
import io.reactivex.Flowable

import javax.inject.Inject
import java.util.concurrent.TimeUnit

@Controller('/api/moderator')
class FeedbackController {

    @Inject
    CommandUseCase commandUseCase

    @Inject
    FindUseCase findUseCase

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("{id}/start")
    boolean start(Authentication authentication, String id){
        Command command = Command.builder().id(id).extra(authentication.name).action(Command.START).build()
        commandUseCase.execute( command )
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("{id}/stop")
    boolean stop(Authentication authentication, String id){
        Command command = Command.builder().id(id).extra(authentication.name).action(Command.STOP).build()
        commandUseCase.execute( command )
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("{id}/reset")
    boolean reset(Authentication authentication, String id){
        Command command = Command.builder().id(id).extra(authentication.name).action(Command.RESET).build()
        commandUseCase.execute( command )
    }

}
