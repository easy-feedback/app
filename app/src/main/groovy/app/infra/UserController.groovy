package app.infra


import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.security.annotation.Secured
import io.micronaut.security.authentication.Authentication
import io.micronaut.security.rules.SecurityRule

@Secured(SecurityRule.IS_AUTHENTICATED)
@Controller("/api/user")
class UserController {

    @Get
    String userDetails(Authentication authentication){
        authentication.attributes['name']
    }
    
}
