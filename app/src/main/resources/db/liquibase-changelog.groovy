package db

databaseChangeLog {
    include file: 'migrations/initialSchema.groovy', relativeToChangelogFile: true
}