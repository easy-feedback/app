package db.migrations

databaseChangeLog {
    changeSet(author: "jorge", id: "1") {
        createTable(tableName: "feedback") {
            column(name: "id", type: "UUID") {
                constraints(primaryKey: "true")
            }

            column(name: "author", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
            column(name: "title", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
            column(name: "raw_questions", type: "VARCHAR(2048)") {
                constraints(nullable: "false")
            }
            column(name: "state", type: "VARCHAR(10)") {
                constraints(nullable: "false")
            }
        }
    }
}
