export default class APIService {

    constructor(axios) {
        this.axios = axios
    }

    loadUser() {
        return this.axios.get(`user`)
    }

    list() {
        return this.axios.get('edit')
    }

    delete(id) {
        return this.axios.delete(`edit/${id}`)
    }

    save(json) {
        if (json.id) {
            return this.axios.put(`edit/${json.id}`, json)
        } else {
            return this.axios.post(`edit`, json)
        }
    }

    startEasyFeedback(uuid) {
        return this.axios.get(`moderator/${uuid}/start`)
    }

    stopEasyFeedback(uuid) {
        return this.axios.get(`moderator/${uuid}/stop`)
    }

    resetEasyFeedback(uuid) {
        return this.axios.get(`moderator/${uuid}/reset`)
    }

    getEasyFeedback(uuid) {
        return this.axios.get(`public/${uuid}`)
    }

    voteEasyFeedback(uuid, question) {
        return this.axios.post(`public/${uuid}/vote`, {
            question: question
        })
    }

    es = null

    stopWatch() {
        if (this.es) {
            this.es.close()
        }
    }

    watchEasyFeedback(uuid, callable) {
        this.stopWatch()
        this.es = new EventSource(`${this.axios.defaults.baseURL}public/${uuid}/sse`);
        this.es.onmessage = function (event) {
            let data = JSON.parse(event.data);
            callable(data);
        };
    }
}