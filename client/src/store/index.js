import Vuex from "vuex";
import router from "@/router";
import axios from 'axios';
import ApiService from "@/services/ApiService";
import Vue from "vue";

const http = axios.create({
    withCredentials: true,
    baseURL: process.env.VUE_APP_API_URL
})
http.interceptors.request.use(function (config) {
    return config;
}, function (err) {
    return Promise.reject(err)
})
http.interceptors.response.use(function (response) {
    return response;
}, function (err) {
    if (err.response.status == 401 || err.response.status == 403) {
        router.push('login')
    }
    return Promise.reject(err)
})

const apiService = new ApiService(http)

Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
        apiService: apiService,
        currentPoll: null,
        user: {
            name: '',
            email: ''
        },
        list: []
    },
    mutations: {
        list(state, list) {
            state.list = list;
        },
        resetCounters(state) {
            var arr = state.currentJson.questions.map(() => 0)
            state.currentVotes = arr;
        },
        updateModel(state, data) {
            state.currentPoll = data
        },
        addQuestion(state) {
            state.currentPoll.questions.push({
                id: state.currentPoll.questions.length,
                question: "Your Question",
                votes: 0
            })
        },
        removeQuestion(state) {
            state.currentPoll.questions.splice(state.currentPoll.questions.length - 1, 1)
        },
        newUser(state, user) {
            state.user.name = user
        }
    },
    actions: {
        async saveModel(context) {
            let response = await context.state.apiService.save(context.state.currentPoll);
            context.commit('updateModel', response.data);
            await context.dispatch('fetchList')
        },
        async fetchUser(context) {
            const user = await apiService.loadUser()
            context.commit('newUser', user.data)
        },
        async fetchList(context) {
            const list = await apiService.list();
            context.commit('list', list.data);
        },
        async removePoll(context, item) {
            await context.state.apiService.delete(item.id);
            await context.dispatch('fetchList')
            if (item.id == context.state.currentPoll.id) {
                context.commit('updateModel', context.state.list[0])
            }
        },
        async startPoll(context) {
            let item = await apiService.startEasyFeedback(context.state.currentPoll.id);
            await context.commit('updateModel', item)
        },
        async stopPoll(context) {
            let item = await apiService.stopEasyFeedback(context.state.currentPoll.id);
            await context.commit('updateModel', item)
        },
        async resetPoll(context) {
            let item = await apiService.resetEasyFeedback(context.state.currentPoll.id);
            await context.commit('updateModel', item)
        },
        async retrievePoll(context, uuid) {
            let item = await apiService.getEasyFeedback(uuid)
            await context.commit('updateModel', item)
        },
        async votePoll(context, option) {
            let item = await apiService.voteEasyFeedback(context.state.currentPoll.id, option);
            await context.commit('updateModel', item)
        }
    }
})

export default store