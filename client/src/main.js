import Vue from 'vue'
import {BootstrapVue, IconsPlugin} from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueRouter from 'vue-router'
import App from './App.vue'
import router from './router'
import store from './store'
import VueQRCodeComponent from 'vue-qrcode-component'

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

// Register the Vue component
Vue.component('qr-code', VueQRCodeComponent)

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
